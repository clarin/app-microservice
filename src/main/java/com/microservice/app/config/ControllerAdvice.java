package com.microservice.app.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;

@EnableWebMvc
@RestControllerAdvice
@Component
public class ControllerAdvice {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> handleException(HttpServletRequest request, Exception exception) throws Exception
    {
        //This is never called (I'm using a debugger and have a breakpoint here)
        return new ResponseEntity<String>(
                "test",
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }
}
