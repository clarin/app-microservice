package com.microservice.app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservice.app.config.ControllerAdvice;
import com.microservice.app.dao.EmployeeDAO;
import com.microservice.app.model.Employee;
import com.microservice.app.model.Employees;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class EmployeeControllerTest {

    private static MockMvc mockMvc;


    @Before
    public void init(){
        EmployeeDAO employeeDao = new EmployeeDAO();
        EmployeeController employeeController= new EmployeeController(employeeDao);

        mockMvc = MockMvcBuilders.standaloneSetup(employeeController)
                .setControllerAdvice(new ControllerAdvice())
                .build();
    }

    @Test
    public void getEmployees() throws Exception {
        Employees list = new Employees();
        list.getEmployeeList().add(new Employee(1, "Lokesh", "Gupta", "howtodoinjava@gmail.com"));
        list.getEmployeeList().add(new Employee(2, "Alex", "Kolenchiskey", "abc@gmail.com"));
        list.getEmployeeList().add(new Employee(3, "David", "Kameron", "titanic@gmail.com"));

        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.get("/employees/"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        final String jsonMessage= mvcResult.getResponse().getContentAsString();

        Assert.assertNotNull(jsonMessage);
    }

    @Test
    public void addEmployee() throws Exception {

        Employee employee= new Employee(2, "Davis", "Calderon","davis@gmail.com");

        ObjectMapper objectMapper = new ObjectMapper();
        String jsobBody = objectMapper.writeValueAsString(employee);

        MvcResult mvcResult= mockMvc.perform(MockMvcRequestBuilders.post("/employees/")
                .header("X-COM-PERSIST","")
                .header("X-COM-LOCATION","ASIA")
                .content(jsobBody)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();

        final String jsonMessage= mvcResult.getResponse().getContentAsString();

        Assert.assertNotNull(jsonMessage);

    }
}